* create shopfront jar file

```
cd shopfront
mvn clean install

```
* create shopfront docker image using Dockerfile

```
docker build -t prabhatiitbhu/djshopfront:1.0 .

```
* push this inage to Docker Hub.

```
docker login

docker push prabhatiitbhu/djshopfront:1.0

```
* Deploying onto Kubernetes for shopfront

```
cd ../kubernetes

kubectl apply -f shopfront-service.yaml

```
* Building the productcatalogue application

```
cd ..
cd productcatalogue/
mvn clean install
docker build -t prabhatiitbhu/djproductcatalogue:1.0 .
docker push prabhatiitbhu/djproductcatalogue:1.0

```
* Building the stockmanager application

```
cd ..
cd productcatalogue/
mvn clean install
docker build -t prabhatiitbhu/djstockmanager:1.0 .
docker push prabhatiitbhu/djstockmanager:1.0

```

* Deploying onto Kubernetes for productcatalogue

```
cd ../kubernetes

kubectl apply -f productcatalogue-service.yaml

```

* Deploying onto Kubernetes for stockmanager

```
cd ../kubernetes

kubectl apply -f stockmanager-service.yaml

```

* get the status for complete structure 

```
kubectl get svc

kubectl get pods

```